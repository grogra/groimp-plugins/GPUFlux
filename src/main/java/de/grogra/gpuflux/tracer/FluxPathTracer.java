package de.grogra.gpuflux.tracer;

import static org.jocl.CL.CL_MEM_READ_WRITE;

import java.io.IOException;

import org.jocl.Sizeof;

import de.grogra.gpuflux.FluxSettings;
import de.grogra.gpuflux.GPUFluxInit;
import de.grogra.gpuflux.jocl.JOCLBuffer;
import de.grogra.gpuflux.jocl.compute.Buffer;
import de.grogra.gpuflux.jocl.compute.ComputeContext;
import de.grogra.gpuflux.jocl.compute.Device;
import de.grogra.gpuflux.jocl.compute.Kernel;
import de.grogra.gpuflux.scene.FluxScene;
import de.grogra.gpuflux.scene.filter.NoneFilter;
import de.grogra.imp3d.View3D;
import de.grogra.util.ProgressMonitor;

public class FluxPathTracer extends FluxTracer {
		
	boolean terminate;  
	
	@Override
	public void trace() throws IOException {
		StringBuffer stats = new StringBuffer();
		log(stats, "<html><pre>");
		log(stats,  "<B>GPUFlux path Tracer</B>\n\n" );
		
		long sceneConstructionTime = 0, sceneSerializationTime = 0;
		long totalRenderTime = 0;
		long totalsamples = 0;
		
		try
		{
			long startTime, time;
			
			setProgress ("Build scene", ProgressMonitor.INDETERMINATE_PROGRESS);
			sceneConstructionTime = System.currentTimeMillis ();
			
			// constuct flux scene
			FluxScene scene = new FluxScene();
			
			// build scene from scene graph
			scene.buildSceneFromGraph( view.getGraph(), (View3D)view, new NoneFilter(), false, this, true, false, FluxSettings.getModelFlatness() );
			
			sceneConstructionTime = System.currentTimeMillis () - sceneConstructionTime;
			
			log(stats,  scene.getLog() );
			log(stats,  scene.getSceneStats() );
			
			setProgress ("Init compute context", ProgressMonitor.INDETERMINATE_PROGRESS);
			sceneSerializationTime = System.currentTimeMillis ();
			
			// init compute context
			ComputeContext computeContext = GPUFluxInit.initComputeContext(false,null);
			log(stats,  computeContext.aquireLog().toString() );
			
			if( !computeContext.valid() )
				return;
			
			// get primary compute device
			Device device = computeContext.getPrimaryDevice();
			log(stats, "Primary Device: \n" + device + "\n");
			
			setProgress ("Serialize scene", ProgressMonitor.INDETERMINATE_PROGRESS);
			
			// construct flux scene serializer
			de.grogra.gpuflux.scene.FluxSceneSerializer serializer = new de.grogra.gpuflux.scene.FluxSceneSerializer();
			
			// construct OCL flux scene
			de.grogra.gpuflux.scene.FluxJOCLScene joclScene = new de.grogra.gpuflux.scene.FluxJOCLScene( serializer, computeContext ); 
			
			//  serialize flux scene
			serializer.serializeScene(scene);
			
			// setup OCL scene, camera and lights
			joclScene.setupOCLScene(useBih);
			joclScene.setupOCLCamera(width, height);
			joclScene.setupOCLLights();
			
			sceneSerializationTime = System.currentTimeMillis () - sceneSerializationTime;
			
			// load kernel
			setProgress ("Load kernel", ProgressMonitor.INDETERMINATE_PROGRESS);
			Kernel kernel = computeContext.createKernel("kernel/pt_kernel.cl", "computeImage", getKernelCompilationArguments(true,false));
			
			// allocate image
			Buffer imageBuffer = device.createBuffer(Sizeof.cl_float4*width*height, CL_MEM_READ_WRITE);
			imageBuffer.clear();
			
			log(stats,  computeContext.getLog() );
			
			log(stats, "<B>Settings</B>\n");	
			log(stats, FluxSettings.getTracerLog());
			log(stats, "        Image size:   " + width + " x " + height + "\n");
			log(stats, "        Total pixels: " + width * height + "\n");
			log(stats, "\n");
			
			// start termination dialog
			terminate = false;
			
			Thread t = new Thread()
			{
				@Override
				public void run() {
		             new MsgBox
		             (null , "Progressive rendering in progress...");
		        	 terminate = true;
		         }
			};
			t.start();
			
			int depth = FluxSettings.getRenderDepth();
			int randomseed = FluxSettings.getRandomSeed();
			double preferredDuration = FluxSettings.getOCLPreferredDuration();
			float minPower = FluxSettings.getRenderMinPower();
			int initialSampleCount = FluxSettings.getOCLInitialSampleCount();
			int maximumSampleCount = FluxSettings.getOCLMaximumSampleCount();
			
			int initialSamples = initialSampleCount / scene.getSampleCount();
			
	        long startDisplayTime = System.currentTimeMillis ();
	        
			int maxsmpl = maximumSampleCount  / scene.getSampleCount();
			int smlprun = Math.min(initialSamples, width*height);
			
			log(stats, "<B>Render Profile</B>\n");
			
			while( !terminate )
			{
				int runsPerScreen = (int)Math.ceil((double)(width*height) / (double)smlprun);
	
				if( BATCH_LOGGING_ENABLED )
				{
					log(stats, "\n<i>Iteration</i>\n");
					log(stats, "Samples per execution: " + smlprun + "\n\n");
				}
				
				double minSamplesPerSecond = Double.MAX_VALUE;
				
				int pixelsPerRun = (int)Math.ceil((double)(width*height) / (double)runsPerScreen);
				int pixel = 0;
				
				for( int i = 0 ; i < runsPerScreen ; i++ )
				{
					setProgress ("Execute kernel", ProgressMonitor.INDETERMINATE_PROGRESS);
					
					int samples = Math.min(pixelsPerRun, width*height - pixel);
					
					startTime = System.currentTimeMillis ();
				
					// set image
					device.setKernelArgMemBuffer(kernel, 1, imageBuffer);
					device.setKernelArgInt(kernel,2, width);
					device.setKernelArgInt(kernel,3, height);
					
					// set pass
					device.setKernelArgInt(kernel,4, samples);
					device.setKernelArgInt(kernel,5, pixel);
							
					// set scene
					joclScene.setKernelArgScene(device, kernel, 6);
			
					// set camera
					joclScene.setKernelArgCamera(device, kernel, 19);
					
					// set seed
					device.setKernelArgInt(kernel,20, randomseed);
					
					// set depth
					device.setKernelArgInt(kernel,21, depth);
					
					// set minimum power
					device.setKernelArgFloat(kernel,22, minPower);
					
					device.finish();
					startTime = System.currentTimeMillis ();
					
					// execute kernel
					device.executeKernel(kernel , samples);
					
					pixel += samples;
					totalsamples += samples;
					
					device.finish();
					time = System.currentTimeMillis () - startTime;
					totalRenderTime += time;
					
					if( BATCH_LOGGING_ENABLED )
					{
						log(stats, "<i>Sample batch</i>\n");
						log(stats, "    Batch size:  " + samples + " samples\n");
						log(stats, "    Render time: " + time + " ms\n");
						if( time > 0 )
							log(stats, "    Performance: " + ((samples / time) / 1000.0) + " MSmpl\n");
					}
		
					double samplesPerSecond = 1000.0 * (samples) / Math.max(time, 1);
					
					minSamplesPerSecond = Math.min( samplesPerSecond, minSamplesPerSecond);
					long timeSinceDisplay = System.currentTimeMillis () - startDisplayTime;
					if( timeSinceDisplay / 1000.0 > preferredDuration )
					{
						displayImage( imageBuffer, device.getByteOrder() );
				        
				        startDisplayTime = System.currentTimeMillis ();
					}
				}
				
				// aim for 500ms per iteration
				int newSmlPerRun = Math.min( maxsmpl , (int) (minSamplesPerSecond * preferredDuration) );
				smlprun = (int) (BATCH_BALANCE_SMOOTH * newSmlPerRun + (1.0-BATCH_BALANCE_SMOOTH) * smlprun);
			}
			
			log(stats,  "Device: " + device.getName() + "\n" );
			log(stats,  "\tTotal samples:     " + totalsamples + "\n");
			log(stats,  "\tSamples per batch: " + smlprun + "\n" );
			log(stats,  "\tTotal trace time:  " + (int)(totalRenderTime * 1000) + " ms\n" );
			log(stats,  "\tSamples per second: " + (totalsamples / (1000.0*1000.0)) / totalRenderTime + " MSmpl/s\n" );
			
			displayImage( imageBuffer, device.getByteOrder() );
			
		}
		finally
		{
			log(stats, "\n<B>Profile Summary</B>\n");
			log(stats, "    Construction time: " + sceneConstructionTime + " ms\n");
			log(stats, "    Serialize time:    " + sceneSerializationTime + " ms\n");
			log(stats, "    Render time:       " + totalRenderTime + " ms\n");
			if( totalRenderTime > 0 )
				log(stats, "    Performance:       " + ((totalsamples / totalRenderTime) / 1000.0) + " MSmpl/s\n");
	        log(stats, "    Device Memory:     " + (JOCLBuffer.getMemoryUsage() / 1024) + " KB\n");
		    
	        setProgress ("Done", ProgressMonitor.DONE_PROGRESS);
	        
			// display statistics
	        log(stats, "</pre></html>");
	        if (!stats.isEmpty()) {
	        	view.getWorkbench().logGUIInfo (stats .toString() );
	        }
		}
	}
}
