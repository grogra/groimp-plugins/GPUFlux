package de.grogra.gpuflux.imp3d.io;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Locale;
import java.util.NoSuchElementException;
import java.util.Scanner;
import java.util.StringTokenizer;

import de.grogra.gpuflux.imp3d.objects.LightDistribution;
import de.grogra.gpuflux.imp3d.objects.LightDistributionResource;
import de.grogra.pf.io.FilterBase;
import de.grogra.pf.io.FilterItem;
import de.grogra.pf.io.FilterSource;
import de.grogra.pf.io.IOFlavor;
import de.grogra.pf.io.ObjectSource;
import de.grogra.pf.io.ReaderSource;

public class IESFilter extends FilterBase implements ObjectSource {

	public static final IOFlavor FLAVOR = IOFlavor.valueOf (LightDistribution.class);

	public IESFilter(FilterItem item, FilterSource source) {
		super (item, source);
		setFlavor (FLAVOR);
	}

	@Override
	public Object getObject() throws IOException {
		Reader in = ((ReaderSource) source).getReader ();
		
		BufferedReader reader = new BufferedReader( in );
		
		final String TILT="TILT";

		// throw all info lines
		while(!reader.readLine().trim().contains(TILT))
		{}
		
		
		String line = reader.readLine();
		int width = Integer.parseInt(line.split(" ", 9)[3]);
		int height= Integer.parseInt(line.split(" ", 9)[4]);
		double luminance[][] = new double[height][width];
		
		if (width<0) {
			System.out.println("width cannot be < 0");
			return null;
		}
		
		// throw one line 
		reader.readLine();
		// throw the angles lines 
		int widthAngles = 0;
		while (widthAngles<width) {
			Scanner sc = new Scanner(reader.readLine());
			sc.useLocale(Locale.US);
			while(sc.hasNextDouble()){
				sc.nextDouble();
				widthAngles++;
			}			
		}
		int heightAngles = 0;
		while (heightAngles<height) {
			Scanner sc = new Scanner(reader.readLine());
			sc.useLocale(Locale.US);
			while(sc.hasNextDouble()){
				sc.nextDouble();
				heightAngles++;
			}			
		}
		
		// then, fill the luminance array
		int x = 0;
		for( int y = 0 ; y < height; y++ )
		{
			x=0;
			while (x < width) {
				Scanner sc = new Scanner(reader.readLine());
				sc.useLocale(Locale.US);
				while(sc.hasNextDouble()){
					luminance[y][x] = sc.nextDouble();
					x++;
				}
			}
		}

		return new LightDistributionResource( luminance );
	}

}
