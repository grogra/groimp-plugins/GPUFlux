package de.grogra.gpuflux.imp3d.objects;

import de.grogra.gpuflux.imp3d.spectral.SpectralCurve;

public interface SpectralLightMap {

	public abstract SpectralCurve getSpectralDistribution();
	
}
