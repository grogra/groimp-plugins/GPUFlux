# GPUFlux Plugin

This plugin provides an implementation of a ray tracer (Fluxtracer) that is computed on GPU.
